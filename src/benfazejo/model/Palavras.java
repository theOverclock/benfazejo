package benfazejo.model;

import java.util.LinkedList;

/**
 * Classe onde serão iniciadas as palavras que serão filtadras
 *
 */
public class Palavras {

    LinkedList palavras = new LinkedList();
    private final String bom = "bom";
    private final String bondoso = "bondoso";
    private final String generoso = "generoso";
    private final String caridoso = "caridoso";
    private final String bemintencionado = "bem-intencionado";
    private final String agradavel = "agradável";
    private final String encantador = "encantador";
    private final String prazeroso = "prazeroso";
    private final String adequado = "adequado";
    private final String apropriado = "apropriado";
    private final String certo = "certo";
    private final String correto = "correto";
    private final String ideal = "ideal";
    private final String educado = "educado";
    private final String decente = "decente";
    private final String justo = "justo";
    private final String competente = "competente";
    private final String eficiente = "eficiente";
    private final String apto = "apto";
    private final String habilitado = "habilitado";
    private final String saboroso = "saboroso";
    private final String seguro = "seguro";
    private final String vantajoso = "vantajoso";
    private final String favoravel = "favorável";
    private final String licito = "lícito";
    private final String legal = "legal";
    private final String lucrativo = "lucrativo";
    private final String benefico = "benéfico";
    private final String curado = "curado";
    private final String livre = "livre";
    private final String ganhou = "ganhou";
    private final String obteve = "obteve";
    private final String adquiriu = "adquiriu";
    private final String conseguiu = "conseguiu";
    private final String acertou = "acertou";
    private final String conquistou = "conquistou";
    private final String avancou = "avançou";
    private final String desenvolveu = "desenvolveu";
    private final String evoluiu = "evoluiu";
    private final String prosperou = "prosperou";
    private final String premiou = "premiou";
    private final String homenageou = "homenageou";
    private final String honrou = "honrou";
    private final String gratificou = "gratificou";
    private final String retribuiu = "retribuiu";
    private final String recompensou = "recompensou";
    private final String vitoria = "vitória";
    private final String conquista = "conquista";
    private final String triunfo = "triunfo";
    private final String exito = "êxito";
    private final String sucesso = "sucesso";
    private final String gloria = "glória";
    private final String vantagem = "vantagem";

    /**
     * Assim que a classe for chamada as palavras serão adicionadas a lista
     */
    public Palavras() {
        palavras.add(bom);
        palavras.add(bondoso);
        palavras.add(generoso);
        palavras.add(caridoso);
        palavras.add(bemintencionado);
        palavras.add(agradavel);
        palavras.add(encantador);
        palavras.add(prazeroso);
        palavras.add(adequado);
        palavras.add(apropriado);
        palavras.add(certo);
        palavras.add(correto);
        palavras.add(ideal);
        palavras.add(educado);
        palavras.add(decente);
        palavras.add(justo);
        palavras.add(competente);
        palavras.add(eficiente);
        palavras.add(apto);
        palavras.add(habilitado);
        palavras.add(saboroso);
        palavras.add(seguro);
        palavras.add(vantajoso);
        palavras.add(favoravel);
        palavras.add(licito);
        palavras.add(legal);
        palavras.add(lucrativo);
        palavras.add(benefico);
        palavras.add(curado);
        palavras.add(livre);
        palavras.add(ganhou);
        palavras.add(obteve);
        palavras.add(adquiriu);
        palavras.add(conseguiu);
        palavras.add(acertou);
        palavras.add(conquistou);
        palavras.add(avancou);
        palavras.add(desenvolveu);
        palavras.add(evoluiu);
        palavras.add(prosperou);
        palavras.add(premiou);
        palavras.add(homenageou);
        palavras.add(honrou);
        palavras.add(gratificou);
        palavras.add(retribuiu);
        palavras.add(recompensou);
        palavras.add(vitoria);
        palavras.add(conquista);
        palavras.add(triunfo);
        palavras.add(exito);
        palavras.add(sucesso);
        palavras.add(gloria);
        palavras.add(vantagem);
    }

    /**
     * Método que retorna uma lista com as palavras que serão filtradas
     *
     * @return lista de palavras
     */
    public LinkedList getLista() {
        return palavras;
    }
}
