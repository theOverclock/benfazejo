package benfazejo.model;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class Noticia {

    private String titulo;
    private Date dataHora; //data de postagem no site original ou data de postagem no nosso site?
    private String resumo; //inicio da noticia
    private String link;
    private String siteOrigem;
    private String foto;
    private double numeroDeAcessos;
    private List<String> tagsDaNoticia;

    public Noticia(String titulo, String dataHora, String resumo, String link,
            String siteOrigem, String foto, double numeroDeAcessos, List<String> tagsDaNoticia) {
        this.titulo = titulo;
        this.dataHora = new Date();
        this.resumo = resumo;
        this.link = link;
        this.siteOrigem = siteOrigem;
        this.foto = foto;
        this.numeroDeAcessos = 0;
        this.tagsDaNoticia = new LinkedList();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + Objects.hashCode(this.link);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Noticia other = (Noticia) obj;
        if (!Objects.equals(this.link, other.link)) {
            return false;
        }
        return true;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Date getDataHora() {
        return dataHora;
    }

    public void setDataHora(Date dataHora) {
        this.dataHora = dataHora;
    }

    public String getResumo() {
        return resumo;
    }

    public void setResumo(String resumo) {
        this.resumo = resumo;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getSiteOrigem() {
        return siteOrigem;
    }

    public void setSiteOrigem(String siteOrigem) {
        this.siteOrigem = siteOrigem;
    }

    public String getFoto() { //precisa do throws
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public double getNumeroDeAcessos() {
        return numeroDeAcessos;
    }

    public void setNumeroDeAcessos(double numeroDeAcessos) { // get numeros de acesso +1
        this.numeroDeAcessos = numeroDeAcessos;
    }

    public List<String> getTagsDaNoticia() { //precisa de throws
        return tagsDaNoticia;
    }

    public void setTagsDaNoticia(List<String> tagsDaNoticia) {
        this.tagsDaNoticia = tagsDaNoticia;
    }
}
